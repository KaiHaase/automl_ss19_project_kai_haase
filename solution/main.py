import numpy as np
import torch.nn as nn

import torch
import torch.utils.data
import matplotlib.pyplot as plt
from utils import AvgrageMeter, accuracy, balanced_accuracy
from models.autoencoder import Autoencoder
import sklearn

import pathlib
from pathlib import Path


def train_autoencoder(train_dataset, test_dataset, target_size, step_size=10, epochs_per_step=5, learning_rate=1e-4, weight_decay=1e-5, batch_size=1024, verbose=False):

    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=True)

    data_dim = train_dataset.tensors[0].shape[1]

    model = Autoencoder(input_size=data_dim, latent_size=data_dim)

    start_size = data_dim

    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=weight_decay)
    criterion = nn.MSELoss()

    history = []
    epoch_counter = 0
    current_latent_size = start_size

    for epoch in range(0, int((start_size - target_size) * epochs_per_step / step_size + epochs_per_step - 1)):
        train_obj = AvgrageMeter()
        test_obj = AvgrageMeter()

        # Reduce latent size every num_epoch epochs
        if epoch_counter == epochs_per_step-1:
            epoch_counter = 0
            
            model.reduce_latent_size(step_size)
            current_latent_size -= step_size
        else:
            epoch_counter += 1
        
        for images, labels in train_loader:
            predictions = model(images)
            loss = criterion(predictions, images)
            
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            
            train_obj.update(loss.item(), len(images))
            
        for images, labels in test_loader:
            predictions = model(images)

            loss = criterion(predictions, images)

            test_obj.update(loss.item(), len(images))
            
        print(f"Epoch {epoch}, latent size {current_latent_size}. LOSS: train {train_obj.avg:.4f} test {test_obj.avg:.4f}.")
        history.append((train_obj.avg, test_obj.avg))

        if verbose:
            # Printing and plotting
            index = np.random.randint(len(images))
            prediction = predictions[index].detach().numpy().reshape(28, 28)
            true = images[index].detach().numpy().reshape(28, 28)

            fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(7, 14))

            ax1.set_title("true image")
            ax1.imshow(true, cmap='gray')

            ax2.set_title("reconstructed image")
            print(np.min(prediction))
            print(np.max(prediction))
            ax2.imshow(prediction, cmap='gray')
            plt.show()

    return model, np.array(history)



def train_classifier(data_path, num_epochs=20, first_hidden=[400], second_hidden=[80], learning_rate=1e-3, weight_decay=1e-4, batch_size=256, dropout_p=0.2, verbose=False):

    hidden_sizes = first_hidden, second_hidden
    train_dataset = torch.load(data_path / 'latent_train')
    test_dataset = torch.load(data_path / 'latent_test')

    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=True)

    data_dim = train_dataset.tensors[0].shape[1]
    
    input_size = data_dim  
    output_size = 49

    model = nn.Sequential(nn.Linear(input_size, hidden_sizes[0]),
                          nn.ReLU(),
                          nn.Dropout(p=dropout_p),
                          nn.Linear(hidden_sizes[0], hidden_sizes[1]),
                          nn.ReLU(),
                          nn.Dropout(p=dropout_p),
                          nn.Linear(hidden_sizes[1], output_size))
             #              nn.Softmax(dim=1))


    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=weight_decay)
    criterion = nn.CrossEntropyLoss()

    history = []
    for epoch in range(num_epochs):
        train_obj = AvgrageMeter()
        test_obj = AvgrageMeter()
        train_acc = AvgrageMeter()
        test_acc = AvgrageMeter()

        for images, labels in train_loader:
            predictions = model(images)
            loss = criterion(predictions, labels)
            
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            
            train_acc.update(accuracy(predictions, labels), len(images))
            train_obj.update(loss.item(), len(images))
            
        for images, labels in test_loader:
            predictions = model(images)
            loss = criterion(predictions, labels)

            test_acc.update(accuracy(predictions, labels), len(images))
            test_obj.update(loss.item(), len(images))
            
        print(f"Epoch {epoch}, LOSS: train {train_obj.avg:.4f} test {test_obj.avg:.4f}. ACCURACY: train {train_acc.avg:.4f} test {test_acc.avg:.4f}")
        history.append((train_obj.avg, test_obj.avg, train_acc.avg, test_acc.avg))
        
    history = np.array(history)
    # return history[-1][1], history[-1][3]
    return model, history