#!/bin/bash
# submit via qaad submit_train.sh
# make sure to set path

#SBATCH -p ml_cpu-ivy # partition (queue)
#SBATCH -t 0-10:00 # time (D-HH:MM)
#SBATCH -a 1-10 # array size 
#SBATCH -e /home/haasek/DL4AC/storage/log/%A.%a.err  # partition (queue)
#SBATCH -o /home/haasek/DL4AC/storage/log/%A.%a.out # partition (queue)

# export PATH="./miniconda3/bin/:$PATH" && source ./miniconda3/bin/activate ./miniconda3/envs/py



# data_path="/data/aad/AIJ/binary_data/"
data_path="/home/haasek/project/data/latent_104/"

echo "SLURM JOB ID $SLURM_JOB_ID"
echo "SLURM ARRAY TASK ID $SLURM_ARRAY_TASK_ID"



/home/haasek/miniconda3/envs/exp/bin/python -m DL4AC.Cluster.BohbWrapperRTD --run_id $SLURM_ARRAY_JOB_ID --array_id $SLURM_ARRAY_TASK_ID --nic_name eth0  --data_path $data_path --min_budget 1 --max_budget 20 --num_iterations=12

