import time
import argparse
import logging
import pathlib
from pathlib import Path
import pickle
import os

import hpbandster.core.nameserver as hpns
import hpbandster.core.result as hpres
from hpbandster.optimizers import BOHB
from worker import MyWorker


def main(args):
    cwd = Path.home()
    store_dir = cwd / "project" / "storage" 

    # Create necessary folder
    store_dir.mkdir(parents=True, exist_ok=True)
    pathlib.Path.mkdir(store_dir / 'log', parents=True, exist_ok=True)
    pathlib.Path.mkdir(store_dir / 'BOHB', parents=True, exist_ok=True)

    options = {'num_iterations': args.num_iterations,
               'min_budget': args.min_budget,
               'max_budget': args.max_budget,
               'eta': 2,
               'show': False,
               'store_dir': store_dir,
               'data_path': Path(args.data_path),
               }

    ns_run_id = args.run_id

    if args.array_id == 1:
        # Set up the logger
        logging.basicConfig(filename=store_dir / 'log' / 'LogFile.log', level=logging.DEBUG)

        logging.info('Store_dir: {}, with {}'.format(store_dir, args.run_id))
        result_logger = hpres.json_result_logger(directory=store_dir / "BOHB", overwrite=True)

        NS = hpns.NameServer(run_id=ns_run_id,
                             nic_name=args.nic_name,
                             working_directory=store_dir / 'BOHB')

        ns_host, ns_port = NS.start()
        logging.info('name server running')

        worker = MyWorker(nameserver=ns_host,
                          nameserver_port=ns_port,
                          run_id=ns_run_id,
                          options=options)

        worker.run(background=True)
        config_space = MyWorker.get_config_space()
        HB = BOHB(configspace=config_space,
                  run_id=ns_run_id,
                  eta=options['eta'],
                  min_budget=options['min_budget'],
                  max_budget=options['max_budget'],
                  host=ns_host,
                  nameserver=ns_host,
                  nameserver_port=ns_port,
                  ping_interval=3600,
                  result_logger=result_logger
                  )

        res = HB.run(n_iterations=options['num_iterations'], min_n_workers=1)

        HB.shutdown(shutdown_workers=True)

        NS.shutdown()

        logging.info('FINISHED RUN')

        with open(os.path.join(store_dir, 'results.pkl'), 'wb') as fh:
            pickle.dump(res, fh)
        # validate(res, options)

    else:
        time.sleep(15)
        host = hpns.nic_name_to_host(args.nic_name)
        w = MyWorker(run_id=ns_run_id, host=host, options=options)
        w.load_nameserver_credentials(store_dir / 'BOHB')
        w.run(background=False)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='HpBandSter Cluster Run')
    parser.add_argument('--run_id', help='unique id to identify the HPB run.', default='HPB_example_2', type=str)
    parser.add_argument('--nic_name', help='name of the Network Interface Card.', default=None, type=str)
    parser.add_argument('--array_id', help='SGE array id to tread one job array as a HPB run.', default=1, type=int)
    parser.add_argument('--dataset', help='name of the dataset', type=str)
    parser.add_argument('--mode', help='here: baseline, schmee or nix', type=str)
    parser.add_argument('--data_path', help='Path to the data', type=str, default='/data/aad/AIJ/binary_data/')
    parser.add_argument('--min_budget', default=1, type=int)
    parser.add_argument('--max_budget', default=20, type=int)
    parser.add_argument('--num_iterations', default=1, type=int)
    parser.add_argument('--censoring', default='baseline', type=str)
    args = parser.parse_args()

    main(args)
