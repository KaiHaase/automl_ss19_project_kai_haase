from hpbandster.core.worker import Worker
from main import train_classifier


import logging

from pathlib import Path

import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH


class MyWorker(Worker):

    def __init__(self, options, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.options = options

    def compute(self, config, budget, **kwargs):
        """
        Method, called by the bohb-framework.
        A network with given configuration and budget will be trained and evaluated.
        :param config: configuration out of the ConfigSpace
        :param budget: number of epoches
        :param kwargs:
        :return: (dict) containing the losses
        """

        config['num_epochs'] = int(budget)
        config['verbose'] = False

        loss, acc = train_classifier(data_path=self.options['data_path'],
                            **config)

        logging.info('stopped')
    

        return ({'loss': loss,
                 'info': {
                          'loss': loss,
                          'accuracy': acc
                         }
                 })

    @staticmethod
    def get_config_space():
        cs = CS.ConfigurationSpace()
        cs.add_hyperparameters(
            [
                CSH.UniformFloatHyperparameter('learning_rate', lower=1e-6, upper=1e-2, log=True),
                CSH.UniformFloatHyperparameter('weight_decay', lower=1e-15, upper=1e-3, log=True),
                CSH.UniformIntegerHyperparameter('first_hidden', lower=200, upper=800, log=False),
                CSH.UniformIntegerHyperparameter('second_hidden', lower=70, upper=600, log=False),
                CSH.UniformIntegerHyperparameter('batch_size', lower=16, upper=512, log=False),
                CSH.UniformFloatHyperparameter('dropout_p', lower=0.0, upper=0.99, log=False),
            ]
        )
        return cs