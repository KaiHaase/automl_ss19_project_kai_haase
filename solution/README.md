# Automatic Dimensionality Reduction Using Autoencoders

## Motivation

- Extract/engineer the meaningful features from high dimensional dataset with low 'information density'
- K-49: Many pixels might not have any effect at all, intrinsic dimensionality is much lower than 28 x 28 

- Usually: 
    - (Domain knowledge)
    - ICA
    - PCA 
    - MDS
    - t-SNE
    - ...
    
- What I personally wanted: Plug and play preprocessing 



# Autoencoders

- Very general approach to finding a subspace encoding

- Have metric for quality of such subspace encoding

  

## The problem with autoencoders: What latent size should I use?

- Guess / domain knowledge / trial and error

- Exhaustive search

    - Upside: Now we really know
    - Downside: Expensive

    ![](/home/kai/.config/Typora/typora-user-images/1568643624215.png){#id .class width=200 } \

- Alternative Approach: Coming up



# Iterative Latent Space Reduction

![](/home/kai/.config/Typora/typora-user-images/1568642791194.png)\ 

Idea:

- Start with latent space = input space
- Initialize model to calculate identity
- Repeat:
    - Delete neuron(s) to reduce latent space
    - Refit model

$\rightarrow$ Much faster exploration of latent space - space. Re-use learned properties



# Results – Latent Space Size


![1568893141742](/home/kai/.config/Typora/typora-user-images/1568893141742.png)\

**K49**: Broad area of negligible loss. For final result: latent size 104. Trained classifier with BOHB, test accuracy of ~86%.

**Avocado**: Dimensions seem completely dispensable, confirms our feature engineering choices in the Kaggle challenge.

**kin8nm**: Highly nonlinear features with (seemingly) no correlation: No level of compression is tolerable.


# Downsides / Ideas

- Fully connected AEs: Loss of proximity context, bad for images
- More elaborate AEs: Tricky to initialize with identity
- Architecture is very rigid, only one part of the architecture is dynamically narrowed
- X, Y correlation is not taken into account 
    $\rightarrow$ Irrelevant white noise feature will be preserved at all cost since we can't reconstruct it
- Does AE loss metric imply performance on actual task?
- Starting in local optimum (identity) might be counterproductive
- Which neuron to delete? → [maybe least activated](https://devblogs.nvidia.com/transfer-learning-toolkit-pruning-intelligent-video-analytics/) 
- Arbitrary distinction between autoencoder and classifier - first solve the problem to then solve the problem. In extreme case, latent space _is_ output of the classification problem.





## 