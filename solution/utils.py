import numpy as np
import torch
from sklearn.metrics import balanced_accuracy_score
# balanced accuracy has a needless warning
import warnings

class AvgrageMeter(object):

    def __init__(self):
        self.reset()

    def reset(self):
        self.avg = 0
        self.sum = 0
        self.cnt = 0

    def update(self, val, n=1):
        self.sum += val * n
        self.cnt += n
        self.avg = self.sum / self.cnt


def accuracy(predictions, labels):
    predictions = predictions.detach().numpy()
    labels = labels.detach().numpy()
    pred_max = np.argmax(predictions, axis=1)
    
    num_correct = np.sum(labels == pred_max)
    return num_correct / len(labels)

def balanced_accuracy(predictions, labels):
    predictions = predictions.detach().numpy()
    labels = labels.detach().numpy()
    pred_max = np.argmax(predictions, axis=1)

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        return balanced_accuracy_score(labels, pred_max)

def make_latent_dataset(model):
    train_x = np.load('data/k49-train-imgs.npz')['arr_0']
    train_y = np.load('data/k49-train-labels.npz')['arr_0']
    test_x = np.load('data/k49-test-imgs.npz')['arr_0']
    test_y = np.load('data/k49-test-labels.npz')['arr_0']

    # map to [0, 1]
    train_x = train_x / 255
    test_x = test_x / 255

    # Shuffle the data
    ids = np.arange(len(train_x))
    np.random.shuffle(ids)

    # train_x = train_x[ids][:50000]
    # train_y = train_y[ids][:50000]

    # Flatten the images
    train_x = train_x.reshape((-1, 28 * 28))
    test_x = test_x.reshape((-1, 28 * 28))

    # Convert all to tensors
    train_x = torch.from_numpy(train_x).type(torch.FloatTensor)
    test_x = torch.from_numpy(test_x).type(torch.FloatTensor)

    train_y = torch.from_numpy(train_y).type(torch.LongTensor)
    test_y = torch.from_numpy(test_y).type(torch.LongTensor)

    latent_train_x = torch.zeros((len(train_x), model.latent_size))
    latent_test_x = torch.zeros((len(test_x), model.latent_size))

    for i in range(len(train_x)):
        if i % 50000 == 0:
            print(f"[{i:6} / {len(train_x)}]")
        pred = model.encode(train_x[i])
        latent_train_x[i] = pred.data

    for i in range(len(test_x)):
        pred = model.encode(test_x[i])
        latent_test_x[i] = pred.data

    # Make datasets
    train = torch.utils.data.TensorDataset(latent_train_x, train_y)
    test = torch.utils.data.TensorDataset(latent_test_x, test_y)

    return train, test
