import torch.nn as nn
import torch
import numpy as np
import copy


def tensor_delete(tens, index, axis):
    mask = np.ones(tens.shape[axis], np.bool)
    mask[index] = 0
    indices = np.arange(tens.shape[axis])[mask]
    if len(tens.shape) == 1:
        return tens[indices]
    if axis == 0:
        return tens[indices, :]
    return tens[:, indices]


class Autoencoder(nn.Module):
    def __init__(self, latent_size=28*28):
        self.latent_size = latent_size
        self.num_filters = [1, 2, 3]
        super(Autoencoder, self).__init__()
        
        # Encoder
        self.conv1 = nn.Conv2d(1, self.num_filters[0], 5, stride=1, padding=2)
        self.conv2 = nn.Conv2d(self.num_filters[0], self.num_filters[1], 5, stride=1, padding=2)
        self.conv3 = nn.Conv2d(self.num_filters[1], self.num_filters[2], 5, stride=1, padding=2)
        self.fc1 = nn.Linear(self.num_filters[2] * 28 * 28, 28 * 28)
        
        # Decoder
        self.fc2 = nn.Linear(28 * 28, self.num_filters[2] * 28 * 28)
        self.conv4 = nn.Conv2d(self.num_filters[2], self.num_filters[1], 5, stride=1, padding=2)
        self.conv5 = nn.Conv2d(self.num_filters[1], self.num_filters[0], 5, stride=1, padding=2)
        self.conv6 = nn.Conv2d(self.num_filters[0], 1, 5, stride=1, padding=2)
        
        self.hardtanh = nn.Hardtanh(0, 1)

        # Initializing the weights to make the model predict the identity
        
        with torch.no_grad():
            
            filter_weights = np.array([[0.0, 0.0, 0.0, 0.0, 0.0], 
                                      [0.0, 0.0, 0.0, 0.0, 0.0], 
                                      [0.0, 0.0, 1.0, 0.0, 0.0], 
                                      [0.0, 0.0, 0.0, 0.0, 0.0],
                                      [0.0, 0.0, 0.0, 0.0, 0.0]])
            
            for i in range(1): 
                self.conv6.weight[i] = torch.nn.Parameter(torch.tensor(copy.deepcopy(filter_weights)))

            for i in range(self.num_filters[0]): 
                self.conv1.weight[i] = torch.nn.Parameter(torch.tensor(copy.deepcopy(filter_weights)))
                self.conv5.weight[i] = torch.nn.Parameter(torch.tensor(copy.deepcopy(filter_weights) / self.num_filters[1]))

            for i in range(self.num_filters[1]): 
                self.conv2.weight[i] = torch.nn.Parameter(torch.tensor(copy.deepcopy(filter_weights) / self.num_filters[0]))
                self.conv4.weight[i] = torch.nn.Parameter(torch.tensor(copy.deepcopy(filter_weights) / self.num_filters[2]))
                
            for i in range(self.num_filters[2]): 
                self.conv3.weight[i] = torch.nn.Parameter(torch.tensor(copy.deepcopy(filter_weights)/ self.num_filters[1]))

            self.conv1.bias = torch.nn.Parameter(torch.zeros(self.conv1.bias.shape))
            self.conv2.bias = torch.nn.Parameter(torch.zeros(self.conv2.bias.shape))
            self.conv3.bias = torch.nn.Parameter(torch.zeros(self.conv3.bias.shape))
            self.conv4.bias = torch.nn.Parameter(torch.zeros(self.conv4.bias.shape))
            self.conv5.bias = torch.nn.Parameter(torch.zeros(self.conv5.bias.shape))
            self.conv6.bias = torch.nn.Parameter(torch.zeros(self.conv6.bias.shape))
            
            self.fc1.bias = torch.nn.Parameter(torch.zeros(self.fc1.bias.shape))
            self.fc2.bias = torch.nn.Parameter(torch.zeros(self.fc2.bias.shape))
            
            
            fc1_weight = np.eye(28*28)
            fc1_weight /= self.num_filters[2]
            fc1_weight = np.tile(fc1_weight, (1, self.num_filters[2]))
            self.fc1.weight = torch.nn.Parameter(torch.tensor(fc1_weight).float())

            fc2_weight = np.eye(28*28)
            fc2_weight = np.tile(fc2_weight, (self.num_filters[2], 1))
            self.fc2.weight = torch.nn.Parameter(torch.tensor(fc2_weight).float())


            
    def encode(self, x):
        x = x.view(-1, 1, 28, 28)
        x = torch.tanh(self.conv1(x))
        x = torch.tanh(self.conv2(x))
        x = torch.tanh(self.conv3(x))
        x = x.view(-1, self.num_filters[2] * 28 * 28)
        x = torch.tanh(self.fc1(x))
        return x
    
    def decode(self, x):
        x = torch.tanh(self.fc2(x))
        x = x.view(-1, self.num_filters[2], 28, 28)
        x = torch.tanh(self.conv4(x))
        x = torch.tanh(self.conv5(x))
        
        x = self.hardtanh(self.conv6(x))
        x = x.view(-1, 28, 28)
        return x

    def forward(self, x):
        x = self.encode(x)
        x = self.decode(x)
        return x
    
    def reduce_latent_size(self, num_dimensions=1):
        for _ in range(num_dimensions):
            self.latent_size -= 1

            # Delete a random neuron
            delete_index = np.random.choice(len(self.fc1.weight))

            self.fc1.weight = torch.nn.Parameter(tensor_delete(self.fc1.weight, delete_index, axis=0))
            self.fc1.bias = torch.nn.Parameter(tensor_delete(self.fc1.bias, delete_index, axis=0))

            self.fc2.weight = torch.nn.Parameter(tensor_delete(self.fc2.weight, delete_index, axis=1))   
