import torch.nn as nn
import torch
import numpy as np


def tensor_delete(tens, index, axis):
    mask = np.ones(tens.shape[axis], np.bool)
    mask[index] = 0
    indices = np.arange(tens.shape[axis])[mask]
    if len(tens.shape) == 1:
        return tens[indices]
    if axis == 0:
        return tens[indices, :]
    return tens[:, indices]


class Autoencoder(nn.Module):
    def __init__(self, input_size, latent_size):
        super(Autoencoder, self).__init__()

        self.latent_size = latent_size

        self.encoder = nn.Sequential(
            nn.Linear(input_size, input_size),
            nn.Tanh(),
            nn.Linear(input_size, latent_size),
            nn.Tanh())
        self.decoder = nn.Sequential(
            nn.Linear(latent_size, input_size),
            nn.Tanh(),
            nn.Linear(input_size, input_size),
            # nn.ReLU())
            nn.Hardtanh(0, 1))
        
        if input_size == latent_size:
            with torch.no_grad():
                # Weights: Just pass
                self.encoder[0].weight = torch.nn.Parameter(torch.eye(input_size))
                self.encoder[2].weight = torch.nn.Parameter(torch.eye(input_size))    
                self.decoder[0].weight = torch.nn.Parameter(torch.eye(input_size))    
                self.decoder[2].weight = torch.nn.Parameter(torch.eye(input_size)) 

                # Biases: Zeros
                self.encoder[0].bias = torch.nn.Parameter(torch.zeros(input_size))
                self.encoder[2].bias = torch.nn.Parameter(torch.zeros(input_size))
                self.decoder[0].bias = torch.nn.Parameter(torch.zeros(input_size))
                self.decoder[2].bias = torch.nn.Parameter(torch.zeros(input_size))

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x
    
    def reduce_latent_size(self, num_dimensions=1):
        for _ in range(num_dimensions):
            self.latent_size -= 1

            # Delete a random neuron
            delete_index = np.random.choice(len(self.encoder[2].weight))

            self.encoder[2].weight = torch.nn.Parameter(tensor_delete(self.encoder[2].weight, delete_index, axis=0))
            self.encoder[2].bias = torch.nn.Parameter(tensor_delete(self.encoder[2].bias, delete_index, axis=0))

            self.decoder[0].weight = torch.nn.Parameter(tensor_delete(self.decoder[0].weight, delete_index, axis=1))      

    def encode(self, x):
        x = x.view(-1, 1, 28, 28)
        x = torch.tanh(self.conv1(x))
        x = torch.tanh(self.conv2(x))
        x = x.view(-1, self.num_filters[1] * 28 * 28)
        x = torch.tanh(self.fc1(x))
        return x  