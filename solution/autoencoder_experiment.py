import torch.nn as nn
import torch
import numpy as np
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import sys

from memory_profiler import profile

from models.conv_autoencoder import Autoencoder
import torch
import torch.utils.data
import matplotlib.pyplot as plt
from utils import AvgrageMeter

import datetime


def tensor_delete(tens, index, axis):
    mask = np.ones(tens.shape[axis], np.bool)
    mask[index] = 0
    indices = np.arange(tens.shape[axis])[mask]
    if len(tens.shape) == 1:
        return tens[indices]
    if axis == 0:
        return tens[indices, :]
    return tens[:, indices]

def main():
    for _ in range(5):
        train_x = np.load('data/k49-train-imgs.npz')['arr_0']
        train_y = np.load('data/k49-train-labels.npz')['arr_0']
        test_x = np.load('data/k49-test-imgs.npz')['arr_0']
        test_y = np.load('data/k49-test-labels.npz')['arr_0']

        # Map to [-1, 1] so that we can use a tanh function
        # train_x = (train_x / 127.5) - 1
        # test_x = (test_x / 127.5) - 1

        # Map to [0, 1] so that we can use a relu function
        train_x = train_x / 255
        test_x = test_x / 255

        ids = np.arange(len(train_x))
        np.random.shuffle(ids)

        train_x = train_x[ids][:10000]
        train_y = train_y[ids][:10000]

        del(ids)

        train_x = torch.from_numpy(train_x).type(torch.FloatTensor)
        test_x = torch.from_numpy(test_x).type(torch.FloatTensor)

        train_y = torch.from_numpy(train_y).type(torch.LongTensor)
        test_y = torch.from_numpy(test_y).type(torch.LongTensor)

        train = torch.utils.data.TensorDataset(train_x, train_y)
        test = torch.utils.data.TensorDataset(test_x, test_y)

        batch_size = 1024

        train_loader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
        test_loader = torch.utils.data.DataLoader(test, batch_size=batch_size, shuffle=True)

        model = Autoencoder()

        learning_rate = 1e-3
        weight_decay = 1e-5

        data_dim = 784
        start_size = data_dim

        # This took 3:00:41

        target_size = 1
        epochs_per_step = 1
        step_size = 1

        perf_per_latent = dict()

        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=weight_decay)
        criterion = nn.MSELoss()

        history = []
        epoch_counter = 0
        current_latent_size = start_size

        a = datetime.datetime.now()

        for epoch in range(0, int((start_size - target_size) * epochs_per_step / step_size + epochs_per_step - 1)):
            train_obj = AvgrageMeter()
            test_obj = AvgrageMeter()

            # Reduce latent size every num_epoch epochs
            if epoch_counter == epochs_per_step-1:
                torch.save(model, "trained_models/conv_autoencoder_" + str(current_latent_size))
                
                epoch_counter = 0

                model.reduce_latent_size(step_size)
                if len(history) > 0:
                    perf_per_latent[current_latent_size] = history[-1]
                current_latent_size -= step_size
            else:
                epoch_counter += 1

            for images, labels in train_loader:
                predictions = model(images)
                loss = criterion(predictions, images)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                train_obj.update(float(loss), len(images))

            for images, labels in test_loader:
                predictions = model(images)
                loss = criterion(predictions, images)

                test_obj.update(float(loss), len(images))

            print(f"Epoch {epoch}, latent size {current_latent_size}. LOSS: train {train_obj.avg:.4f} test {test_obj.avg:.4f}.")
            history.append(float(test_obj.avg))


            # Printing and plotting
            index = np.random.randint(len(images))
            prediction = predictions[index].detach().numpy().reshape(28, 28)
            true = images[index].detach().numpy().reshape(28, 28)

            """fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(7, 14))

            ax1.set_title("true image")
            ax1.imshow(true, cmap='gray')

            ax2.set_title("reconstructed image")
            ax2.imshow(prediction, cmap='gray')
            plt.show()
            """

        b = datetime.datetime.now()
        print(b-a)

        print(history)
        with open('history_' + str(_) +'.txt', 'w') as f:
            for res in history:
                f.write("%s\n" % res)



if __name__ == "__main__":
    main()